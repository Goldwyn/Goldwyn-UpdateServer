require('dotenv').config();

const port = process.env.PORT || 900;

const http = require('http'),
    express = require('express'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    app = express();

require('./service/sql-service');

app.enable('trust proxy');
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));

const corsOptions = {
    origin: ['*'],
    methods: ['GET', 'POST', 'OPTIONS', 'PUT', 'PATCH', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    exposedHeaders: ['Content-Disposition'],
    credentials: true,
    preflightContinue: false,
};

const standardCorsOptions = {
    origin: '*',
    methods: ['GET']
};

const versions = require('');

app.use();
app.use(require('./middleware/sql-getter'));

const auth = require('./route/auth/route')(express.Router());
app.use('/admin', auth);

app.use(require('./middleware/sql-releaser'));

http.createServer(app).listen(port, () => console.log("Goldwyn-UpdateServer successfully starts and is listening on port " + port + ". " + (config.prod === true ? 'Production' : 'Developpement') + " version"));
